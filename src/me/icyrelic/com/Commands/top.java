package me.icyrelic.com.Commands;



import me.icyrelic.com.LegendaryTeleport;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class top implements CommandExecutor {
	
	LegendaryTeleport plugin;
	public top(LegendaryTeleport instance) {

		plugin = instance;

		}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("top")) {
			
			if(sender.hasPermission("LegendaryTeleport.top")){

				if(sender instanceof Player){
					Player p = (Player) sender;
					World world = p.getWorld();
					int x = p.getLocation().getBlockX();
					int z = p.getLocation().getBlockZ();
					Block b = p.getWorld().getHighestBlockAt(x,z);
							
					Location loc = new Location(world, b.getX(), b.getY(), b.getZ());
					loc.add(0.5, 0, 0.5);
					loc.setPitch(p.getLocation().getPitch());
					loc.setYaw(p.getLocation().getYaw());
							
							
							
							
					p.teleport(loc);
							
					p.sendMessage(ChatColor.GREEN+"Teleported to the top");
							

				}else{
					sender.sendMessage(ChatColor.RED + "ERROR: Must be a player!");
				}
				
			}else{
				sender.sendMessage(ChatColor.RED + "You dont have permission!");
			}
			
		}
		return true;
	}
	
	

}
