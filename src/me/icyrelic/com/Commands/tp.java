package me.icyrelic.com.Commands;


import java.util.List;

import me.icyrelic.com.LegendaryGodMode;
import me.icyrelic.com.LegendaryTeleport;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class tp implements CommandExecutor {
	
	LegendaryTeleport plugin;
	public tp(LegendaryTeleport instance) {

		plugin = instance;

		}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("tp")) {
			
			if(sender.hasPermission("LegendaryTeleport.tp")){

				if(sender instanceof Player){
					Player p = (Player) sender;
					if (args.length == 1) { 
						
						@SuppressWarnings("deprecation")
						List<Player> possible = Bukkit.matchPlayer(args[0]);
						
						if(possible.size() == 1){
							@SuppressWarnings("deprecation")
							Player target = (Player) plugin.getServer().getPlayer(possible.toString().replace("[CraftPlayer{name=", "").replace("}]", ""));
							
							p.sendMessage(ChatColor.GREEN + "Teleported to "+target.getName());
							
							if(!p.hasPermission("LegendaryTeleport.bypass") || target.hasPermission("LegendaryTeleport.bypass")){
								target.sendMessage(ChatColor.GREEN+p.getName() + " has teleported to you");
							}
							if(plugin.getConfig().getBoolean("temp_god_mode")){
								 getLegendaryGodMode().api.addTempGod(p, target);
							 }
							
							p.teleport(target);
								
							
						}else if(possible.size() > 1){
							p.sendMessage(ChatColor.RED + "ERROR: multiple players found");
						}else if(possible.size() == 0){
							p.sendMessage(ChatColor.RED + "Error: player not found");
						}
						
						

					}else if (args.length == 2) {
						@SuppressWarnings("deprecation")
						List<Player> possible = Bukkit.matchPlayer(args[0]);
						
						if(possible.size() == 1){
							@SuppressWarnings("deprecation")
							List<Player> possible1 = Bukkit.matchPlayer(args[1]);
							
							if(possible1.size() == 1){
								@SuppressWarnings("deprecation")
								Player p1 = (Player) plugin.getServer().getPlayer(possible.toString().replace("[CraftPlayer{name=", "").replace("}]", ""));
								@SuppressWarnings("deprecation")
								Player target = (Player) plugin.getServer().getPlayer(possible1.toString().replace("[CraftPlayer{name=", "").replace("}]", ""));
								
								
								p1.sendMessage(ChatColor.GREEN + p.getName()+" Teleported you to "+target.getName());
								
								target.sendMessage(ChatColor.GREEN + p.getName()+" Teleported "+p1.getName()+" to you");
								
									
								LegendaryGodMode lg = (LegendaryGodMode) plugin.getServer().getPluginManager().getPlugin("LegendaryGodMode");
								
								lg.api.addTempGod(p, target);
								
								p1.teleport(target);
								
							}else if(possible1.size() > 1){
								p.sendMessage(ChatColor.RED + "ERROR: multiple players found");
							}else if(possible1.size() == 0){
								p.sendMessage(ChatColor.RED + "Error: player not found");
							}
							

								
							
						}else if(possible.size() > 1){
							p.sendMessage(ChatColor.RED + "ERROR: multiple players found");
						}else if(possible.size() == 0){
							p.sendMessage(ChatColor.RED + "Error: player not found");
						}
						
						
					}else{
						sender.sendMessage(ChatColor.RED + "Usage: /tp <player>");
					}
					
				}else{
					sender.sendMessage(ChatColor.RED + "ERROR: Must be a player!");
				}
				
			}else{
				sender.sendMessage(ChatColor.RED + "You dont have permission!");
			}
			
		}
		return true;
	}
	
	
	private LegendaryGodMode getLegendaryGodMode() {
	    Plugin pl = plugin.getServer().getPluginManager().getPlugin("LegendaryGodMode");
	 
	    if (pl == null || !(pl instanceof LegendaryGodMode)) {
	        return null;
	    }
	 
	    return (LegendaryGodMode) pl;
	}

}
