package me.icyrelic.com.Commands;


import java.util.List;

import me.icyrelic.com.LegendaryTeleport;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class tpa implements CommandExecutor {
	
	LegendaryTeleport plugin;
	public tpa(LegendaryTeleport instance) {

		plugin = instance;

		}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("tpa")) {
			
			if(sender.hasPermission("LegendaryTeleport.tpa")){

				if(sender instanceof Player){
					Player p = (Player) sender;
					if (args.length == 1) { 
						@SuppressWarnings("deprecation")
						List<Player> possible = Bukkit.matchPlayer(args[0]);
						
						if(possible.size() == 1){
							@SuppressWarnings("deprecation")
							Player target = (Player) plugin.getServer().getPlayer(possible.toString().replace("[CraftPlayer{name=", "").replace("}]", ""));
							
							
							p.sendMessage(possible.toString().replace("[CraftPlayer{name=", "").replace("}]", ""));
								p.sendMessage(ChatColor.GOLD + "Teleport request sent to "+ChatColor.RED+target.getName()+ChatColor.GOLD+"!");
								
								target.sendMessage(ChatColor.RED+p.getName()+ChatColor.GOLD+" has requested to teleport to you");
								target.sendMessage(ChatColor.GOLD+"To Teleport, type "+ChatColor.RED+"/tpaccept");
								target.sendMessage(ChatColor.GOLD+"To Deny this request, type "+ChatColor.RED+"/tpdeny");
								
								int x = plugin.getConfig().getInt("tpa_expire");
								
								p.sendMessage(ChatColor.GOLD+"Teleport request will expire in "+ChatColor.RED+x+" seconds");
								target.sendMessage(ChatColor.RED+p.getName()+"'s "+ChatColor.GOLD+" teleport request will expire in "+ChatColor.RED+x+" seconds");
								
								
								if(plugin.tpaRequests.values().contains(p.getName())){
									plugin.tpaRequests.values().remove(p.getName());
								}
								
								if(plugin.tpaRequests.containsKey(target.getName())){
									plugin.tpaRequests.remove(target.getName());
					    		}
								if(plugin.type.containsKey((target.getName()+","+p.getName()))){
									
									plugin.type.remove((target.getName()+","+p.getName()));
								}
								
								plugin.type.put(target.getName()+","+p.getName(), "tpa");
								
								plugin.tpaRequests.put(target.getName(), p.getName());
								
								
								plugin.tpaExpire(p);
								
								
								
							
						}else if(possible.size() > 1){
							p.sendMessage(ChatColor.RED + "ERROR: multiple players found");
						}else if(possible.size() == 0){
							p.sendMessage(ChatColor.RED + "Error: player not found");
						}
						
						
					}else{
						sender.sendMessage(ChatColor.RED + "Usage: /tpa <player>");
					}
					
				}else{
					sender.sendMessage(ChatColor.RED + "ERROR: Must be a player!");
				}
				
			}else{
				sender.sendMessage(ChatColor.RED + "You dont have permission!");
			}
			

			
			
		}
		return true;
	}
	
}
