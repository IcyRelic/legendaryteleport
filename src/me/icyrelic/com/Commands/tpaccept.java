package me.icyrelic.com.Commands;


import java.util.List;

import me.icyrelic.com.LegendaryTeleport;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class tpaccept implements CommandExecutor {
	
	LegendaryTeleport plugin;
	public tpaccept(LegendaryTeleport instance) {

		plugin = instance;

		}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("tpaccept")) {
			
			if(sender.hasPermission("LegendaryTeleport.tpaccept")){

				if(sender instanceof Player){
					Player p = (Player) sender;
					
					if(plugin.tpaRequests.containsKey(p.getName())){
						@SuppressWarnings("deprecation")
						List<Player> possible = Bukkit.matchPlayer(plugin.tpaRequests.get(p.getName()));
						
						if(possible.size() == 1){
							@SuppressWarnings("deprecation")
							Player target = (Player) plugin.getServer().getPlayer(possible.toString().replace("[CraftPlayer{name=", "").replace("}]", ""));
							
							p.sendMessage(ChatColor.GOLD + "Teleport Request Accepted");
							target.sendMessage(ChatColor.RED + p.getName()+" accepted your request");
							String type = "";
							if(plugin.type.containsKey(p.getName()+","+target.getName())){
								type = plugin.type.get(p.getName()+","+target.getName());
								int delay = plugin.getConfig().getInt("teleport_delay");
								
								p.sendMessage(ChatColor.GRAY+"Teleport will commence in "+delay+" seconds");
								target.sendMessage(ChatColor.GRAY+"Teleport will commence in "+delay+" seconds");
								plugin.teleport(p, target, type);
							}
								
								
							
						}else{
							p.sendMessage(ChatColor.RED + "ERROR: unknown error try again");
						}
						
					}else{
						p.sendMessage(ChatColor.RED + "ERROR: No pending requests!");
					}

					
					
				}else{
					sender.sendMessage(ChatColor.RED + "ERROR: Must be a player!");
				}
				
			}else{
				sender.sendMessage(ChatColor.RED + "You dont have permission!");
			}
			

			
			
		}
		return true;
	}
	

}
