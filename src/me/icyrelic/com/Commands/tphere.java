package me.icyrelic.com.Commands;


import java.util.List;

import me.icyrelic.com.LegendaryGodMode;
import me.icyrelic.com.LegendaryTeleport;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class tphere implements CommandExecutor {
	
	LegendaryTeleport plugin;
	public tphere(LegendaryTeleport instance) {

		plugin = instance;

		}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("tphere")) {
			
			if(sender.hasPermission("LegendaryTeleport.tphere")){

				if(sender instanceof Player){
					Player p = (Player) sender;
					if (args.length == 1) { 
						
						
						@SuppressWarnings("deprecation")
						List<Player> possible = Bukkit.matchPlayer(args[0]);
						
						if(possible.size() == 1){
							
							@SuppressWarnings("deprecation")
							Player target = (Player) plugin.getServer().getPlayer(possible.toString().replace("[CraftPlayer{name=", "").replace("}]", ""));
							
							target.sendMessage(ChatColor.GREEN + "Teleported to "+p.getName());
							p.sendMessage(ChatColor.GREEN+target.getName() + " has teleported to you");
								
							
							if(plugin.getConfig().getBoolean("temp_god_mode")){
								 getLegendaryGodMode().api.addTempGod(p, target);
							 }							
							target.teleport(p);
								
							
						}else if(possible.size() > 1){
							p.sendMessage(ChatColor.RED + "ERROR: multiple players found");
						}else if(possible.size() == 0){
							p.sendMessage(ChatColor.RED + "Error: player not found");
						}
						
						

					}else{
						sender.sendMessage(ChatColor.RED + "Usage: /tphere <player>");
					}
					
				}else{
					sender.sendMessage(ChatColor.RED + "ERROR: Must be a player!");
				}
				
			}else{
				sender.sendMessage(ChatColor.RED + "You dont have permission!");
			}
			
		}
		return true;
	}
	
	
	private LegendaryGodMode getLegendaryGodMode() {
	    Plugin pl = plugin.getServer().getPluginManager().getPlugin("LegendaryGodMode");
	 
	    if (pl == null || !(pl instanceof LegendaryGodMode)) {
	        return null;
	    }
	 
	    return (LegendaryGodMode) pl;
	}

}
