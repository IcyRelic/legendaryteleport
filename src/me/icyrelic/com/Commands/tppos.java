package me.icyrelic.com.Commands;



import me.icyrelic.com.LegendaryTeleport;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class tppos implements CommandExecutor {
	
	LegendaryTeleport plugin;
	public tppos(LegendaryTeleport instance) {

		plugin = instance;

		}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("tppos")) {
			
			if(sender.hasPermission("LegendaryTeleport.tppos")){

				if(sender instanceof Player){
					Player p = (Player) sender;
					if (args.length == 3) { 
						String x = args[0];
						String y = args[1];
						String z = args[2];
						World world = p.getWorld();
						if(validCords(x,y,z)){
							
							Block b = p.getWorld().getHighestBlockAt(Integer.parseInt(x), Integer.parseInt(z));
							
							Location loc = new Location(world, Integer.parseInt(x), b.getY(), Integer.parseInt(z));
							loc.add(0.5, 0, 0.5);
							loc.setPitch(p.getLocation().getPitch());
							loc.setYaw(p.getLocation().getYaw());
							
							
							
							
							p.teleport(loc);
							
							p.sendMessage(ChatColor.GREEN+"Teleported to: "+x+", " + b.getY() + ", " + z);
							
						}else{
							sender.sendMessage(ChatColor.RED + "ERROR: Invalid Coordinates!");
						}

					}else{
						sender.sendMessage(ChatColor.RED + "Usage: /tppos <x> <y> <z>");
					}
					
				}else{
					sender.sendMessage(ChatColor.RED + "ERROR: Must be a player!");
				}
				
			}else{
				sender.sendMessage(ChatColor.RED + "You dont have permission!");
			}
			
		}
		return true;
	}
	
	private static boolean validCords(String x, String y, String z) {
	    try { 
	        Integer.parseInt(x);
	        Integer.parseInt(y);
	        Integer.parseInt(z);
	    } catch(NumberFormatException e) { 
	        return false; 
	    }
	    // only got here if we didn't return false
	    return true;
	}
	
	

}
