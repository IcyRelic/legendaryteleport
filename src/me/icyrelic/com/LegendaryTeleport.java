package me.icyrelic.com;

import java.util.HashMap;


import me.icyrelic.com.Commands.*;
import me.icyrelic.com.Data.Updater;
import me.icyrelic.com.Data.Updater.UpdateResult;
import me.icyrelic.com.Data.Updater.UpdateType;

import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class LegendaryTeleport extends JavaPlugin{
	
	public HashMap<String, String> tpaRequests = new HashMap<String, String>();
	public HashMap<String, String> type = new HashMap<String, String>();
	public String prefix = (ChatColor.WHITE+"[" + ChatColor.GREEN + "LegendaryTeleport" + ChatColor.WHITE + "] ");
	
	
	public void onEnable(){
		ConsoleCommandSender console = getServer().getConsoleSender();
		if(getConfig().getBoolean("temp_god_mode") && getLegendaryGodMode() == null){
			console.sendMessage(prefix+"LegendaryGodMode Plugin is Required To Use Temp God Mode!");
			console.sendMessage(prefix+"Shutting Down!");
			getServer().getPluginManager().disablePlugin(this);
		}else{
			loadConfiguration();
			
			if(getConfig().getBoolean("AutoUpdate")){
				Updater check = new Updater(this, 62810, this.getFile(), UpdateType.NO_DOWNLOAD, true);
			
				if (check.getResult() == UpdateResult.UPDATE_AVAILABLE) {
					console.sendMessage(prefix+"New Version Available! "+ check.getLatestName());
			    
					Updater download = new Updater(this, 62810, this.getFile(), UpdateType.DEFAULT, true);
				
					if(download.getResult() == UpdateResult.SUCCESS){
						console.sendMessage(prefix+"Successfully Updated Please Restart To Finalize");
					}
			    
				}else{
					console.sendMessage(prefix+"You are currently running the latest version of LegendaryTeleport");
				}
			}
			
			getCommand("tpa").setExecutor(new tpa(this));
			getCommand("tpahere").setExecutor(new tpahere(this));
			getCommand("tpaccept").setExecutor(new tpaccept(this));
			getCommand("tpdeny").setExecutor(new tpdeny(this));
			getCommand("tp").setExecutor(new tp(this));
			getCommand("tphere").setExecutor(new tphere(this));
			getCommand("tppos").setExecutor(new tppos(this));
			getCommand("top").setExecutor(new top(this));

		}
		

		
	}
	
	private LegendaryGodMode getLegendaryGodMode() {
	    Plugin pl = getServer().getPluginManager().getPlugin("LegendaryGodMode");
	 
	    if (pl == null || !(pl instanceof LegendaryGodMode)) {
	        return null;
	    }
	 
	    return (LegendaryGodMode) pl;
	}
	
	
	
	public void teleport(final Player target, final Player p, final String type){
		int delay = getConfig().getInt("teleport_delay");
		  getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
			  public void run() {
				  
				  
				  if(type.equals("tpa")){
					  p.sendMessage(ChatColor.GREEN + "Teleported to "+target.getName()+"!");
					  target.sendMessage(ChatColor.GREEN + p.getName()+" has teleported to you!");
					  
					  if(tpaRequests.values().contains(p.getName())){
						  tpaRequests.values().remove(p.getName());
					  }
					  if(tpaRequests.values().contains(target.getName())){
						  tpaRequests.values().remove(target.getName());
					  }
					 
						
					 if(getConfig().getBoolean("temp_god_mode")){
						 getLegendaryGodMode().api.addTempGod(p, target);
					 }
					  
					  if(tpaRequests.values().contains(p.getName())){
						  tpaRequests.values().remove(p.getName());
					  }
					  
					  p.teleport(target);
					  
					  
					  
				  }else if(type.equals("tpahere")){
					  target.sendMessage(ChatColor.GREEN + "Teleported to "+p.getName()+"!");
					  p.sendMessage(ChatColor.GREEN + target.getName()+" has teleported to you!");
					  
					  if(tpaRequests.values().contains(p.getName())){
						  tpaRequests.values().remove(p.getName());
					  }
					  if(tpaRequests.values().contains(target.getName())){
						  tpaRequests.values().remove(target.getName());
					  }
					  
					  LegendaryGodMode lg = (LegendaryGodMode) getServer().getPluginManager().getPlugin("LegendaryGodMode");
						
					  lg.api.addTempGod(p, target);
					  
					  if(tpaRequests.values().contains(p.getName())){
						  tpaRequests.values().remove(p.getName());
					  }
					  
					  target.teleport(p);
				  }
				  
				  
				  
				  
			  }
			}, 20*delay);
	}
	
	private void loadConfiguration(){
	    getConfig().options().copyDefaults(true);
	    saveConfig();
	
	}
	
	public void tpaExpire(final Player p){
		  getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
			  public void run() {
				  
				  
				  if(tpaRequests.values().contains(p.getName())){
					  p.sendMessage(ChatColor.RED + "Teleport Request Expired");
					  tpaRequests.values().remove(p.getName());
				  }
				  
				  
				  
			  }
			}, 20*getConfig().getInt("tpa_expire"));
	}

}
